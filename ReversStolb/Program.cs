﻿using System;
using static System.Console;

namespace ReversStolb
{
    class Program
    {
        static void Main()
        {
            int n = 3, m = 5, bufer;
            Random rnd = new Random();
            int[,] mas = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = rnd.Next(0, 10);
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Write("{0, -5}", mas[i, j]);
                }
                WriteLine();
            }
            //реверс
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < (m / 2); j++)
                {
                    bufer = mas[i, j];
                    mas[i, j] = mas[i, m - 1 - j];
                    mas[i, m - 1 - j] = bufer;
                }
            }
            //вывод реверса
            WriteLine("\n\n");

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Write("{0, -5}", mas[i, j]);
                }
                WriteLine();
            }

            ReadKey();
        }
    }
}
