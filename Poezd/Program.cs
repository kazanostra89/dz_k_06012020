﻿using System;
using static System.Console;

namespace Poezd
{
    class Program
    {
        static void Main()
        {
            int n = 18, m = 36, position, numberVagon;
            bool proverChisel;
            Random rnd = new Random();
            int[,] mas = new int[n, m];
            int[] nas = new int[n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = rnd.Next(0, 2);
                }
            }
            
            for (int i = 0; i < n; i++)
            {
                position = 0;
                for (int j = 0; j < m; j++)
                {
                    if (mas[i, j] == 0)
                    {
                        position++;
                    }
                }
               //WriteLine("В вагоне №" + (i + 1) + " свободных мест = " + position);    самопроверка
                nas[i] = position;
            }
                        
            do
            {
                Write("Введите номер вагона № от 1 до 18: ");
                proverChisel = int.TryParse(ReadLine(), out numberVagon);
            } while (proverChisel == false || numberVagon < 1 || numberVagon > 18);

            WriteLine("\nКоличество свободных мест в вагоне №" + numberVagon + " = " + (nas[numberVagon - 1]));

            ReadKey();
        }
    }
}
